#!/usr/bin/env python

import socket

class Quote:
  def __init__(self, symbol, exchange, bid, offer):
    self.symbol = symbol
    self.exchange = exchange
    self.bid = bid
    self.offer = offer

TCP_IP = '199.83.14.77'
TCP_PORT = 7777
BUFFER_SIZE = 1024
QuotesDict = {} 

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))
buffer = b""

def calc_and_print_nbbo_for_symbol(symbol, quotesDict):
    bidList = []
    offerList = []
    for k,v in quotesDict.items(): 
        if k.startswith(symbol):
            #print(q)
            bidList.append(v.bid)
            offerList.append(v.offer)
    
    print("*** " + symbol + " NBBO = " + str(max(bidList)) + " @ " + str(min(offerList)))

while True:
    data, addr = s.recvfrom(BUFFER_SIZE)
    buffer += data
    if not data.endswith(b"\n"):
        continue
    if data:
        #print(buffer)
        lines = data.split(b"\n")
        lines = filter(None, lines)
        for line in lines:
            if line.startswith(b"Q|IBM"):
                print(line)
                arr = line.decode().split("|")
                q = Quote(arr[1], arr[2], int(arr[3]), int(arr[4]))
                key = arr[1] + "|" + arr[2]
                QuotesDict[key] = q
                calc_and_print_nbbo_for_symbol(arr[1], QuotesDict)

        buffer = b""
    else:
        break
